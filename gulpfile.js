const gulp = require('gulp');
const notify = require("gulp-notify");
const babel = require('gulp-babel');
const browserify = require('browserify');

// Scripts
gulp.task('build', function() {
    return gulp.src("./src/**")
        .pipe(babel())
        .pipe(gulp.dest('dist'))
        .pipe(notify("Hello Gulp!"));
});

gulp.task('default', function() {
    return gulp.src("./src/**")
        .pipe(browserify())
        .pipe(gulp.dest('dist'))
        //.pipe(babel())

        .pipe(notify("complete!"));
});