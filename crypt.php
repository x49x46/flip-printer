<?php
/**
 * Created by PhpStorm.
 * User: x49x46
 * Date: 4/18/19
 * Time: 9:32 AM
 */

$KEY = 'key.pem';

$req = $_POST['request'];

if ($req and file_exists($KEY)) {
    $content = file_get_contents($KEY);
    $privateKey = openssl_get_privatekey($content, 'ifthen989804');

    $signature = null;
    openssl_sign($req, $signature, $privateKey);

    if ($signature) {
        header("Content-type: text/plain");
        echo base64_encode($signature);
        exit(0);
    }
}

echo '<h1>Error signing message</h1>';
exit(1);