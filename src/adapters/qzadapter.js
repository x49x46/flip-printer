var qzAdapter = function() {
    var base = new baseAdapter();

    base._connected = false;
    base._promise = null;
    base._cert = null;
    base._scale = false;
    base._in_progress = false;
    base._queue = [];


    /**
     * Устанавливает флаг масштабироания
     *
     * @param scale bool
     */
    base.setScale = function(scale) {
        base._scale = scale;
    };

    base.connect = function(callback = null)
    {
        var self = this;

        if (!qz.websocket.isActive()) {

            qz.security.setCertificatePromise(function (resolve, reject) {
                if (self.cert) {
                    console.log('sefl cert used');
                    resolve(self.cert);
                } else {
                    console.log('request');
                    //Preferred method - from server
                    $.ajax({ url: "/external/flip-printer/cert.pem", cache: false, dataType: "text" }).then(resolve, reject);
                }
            });

            qz.security.setSignaturePromise(function(toSign) {
                return function(resolve, reject) {
                    $.post("/external/flip-printer/crypt.php", {request: toSign}).then(resolve, reject);
                };
            });

            this._promise = new Promise((resolve, reject) => {
                qz.api.showDebug(false);
                qz.websocket.connect({retries: 5, delay: 1, usingSecure: false}).then(function () {

                    qz.printers.find().then(function(data) {
                        //console.log('qz founded printers: ', data);
                        self.printers = data;
                    }).catch(function(data) {
                        console.error(data);
                    });

                    self._connected = true;
                    resolve(true);
                }).catch(function (error) {
                    console.warn(error);
                    reject(false);
                });
            });

            if (callback) {
                this._promise.then(function() {
                    callback(true);
                }).catch(function() {
                    callback(false);
                });
            }

        } else {
            return self._connected = true;
        }
    };

    base.setCert = function(cert)
    {
        this._cert = cert;
        this.initCrypto();
    };

    base.initCrypto = function()
    {
        var self = this;
        console.log('init: ', self);
    };

    base.isConnected = function()
    {
        return this._connected;
    };

    base.inConnect = function()
    {
        return !!this._promise;
    };

    base.getPrinters = function()
    {
        return this.printers; //qz.printers.find();
    };

    base.pageHead = function(html) {
        var head = html.match(/<head>(.*?)<\/head>/);

        if (head) {
            return head[0];
        }

        return false;
    };

    base.pageBreak = function (html, break_start = null, break_end = null) {
        if (!break_start) {
            break_start = '<!--page-break-start-->';
        }

        if (!break_end) {
            break_end = '<!--page-break-end-->';
        }

        var regexp = new RegExp(break_start + '(.*?)' + break_end, 'g');

        return html.match(regexp);
    };

    base.exec = function(html, config)
    {
        var self = this;

        if (!this._printer_name) {
            return false;
        }

        var page_head = '';
        var page_break = this.pageBreak(html);

        if (page_break) {
            page_head = this.pageHead(html);
        } else {
            page_break = [];
            page_break.push(html);
        }

        for (var i = 0; i < page_break.length; i ++) {

            var qz_config = {
                scaleContent: config.scale ? config.scale : false,
                //interpolation: 'bilinear',
                //colorType: 'blackwhite',
                //density: 203,
                units: this.getUnits(),
                orientation: config.orientation,//this.getOrientation(),
                jobName: 'qz_' + self._printer_name + '_' + (new Date()).getTime() + '_' + Math.round(Math.random() * 100) + '_w_' + i,
                margins: {
                    top: config.marginTop,//this.getMarginTop(),
                    right: config.marginRight,//this.getMarginRight(),
                    bottom: config.marginBottom,//this.getMarginBottom(),
                    left: config.marginLeft//this.getMarginLeft()
                }
            };

            if (config.width && config.height) {
                qz_config.size = {
                    width: config.width,
                    height: config.height
                };
            }

            var data = [{
                // type: 'html',
                // format: 'plain',
                // data: html
                type: 'pixel',
                format: 'html',
                flavor: 'plain',
                data: page_head + page_break[i]
            }];

            this._queue.push({
                printer: config.printer_name,
                config: qz_config,
                data: data
            });
        }

        //IF: если печать уже идёт то текущие данные ждут в очереди
        if (this._in_progress) {
            return true;
        }

        //IF: Печать
        var qzPrint = function() {

            //IF: есмли очередь не пуста то печатаем оттуда
            if (self._queue.length === 0) {
                return;
            }

            var print_data = self._queue.shift();
            var config = print_data.config;
            var data = print_data.data;
            var printer = print_data.printer;

            config = qz.configs.create(printer, config);
            self._in_progress = true;

            qz.print(config, data).then(function() {
                if (self._queue.length === 0) {
                    console.log('queue complete!');
                    self._in_progress = false;
                } else {
                    qzPrint();
                }
            }).catch(function (e) {
                console.error(e);
            });
        };

        qzPrint();
    };

    base.print = function(html, config) {
        var self = this;

        if (!html) {
            html = document.documentElement.innerHTML;
        }

        if (this.isConnected()) {
            this.exec(html, config);
        } else if (this.inConnect()) {
            this._promise.then(function(resolve) {
                if (resolve) {
                    self.exec(html, config);
                }
            });
        } else {
            //if QZ reload
            this.connect(function(){
                self.exec(html, config);
            });
        }
    };

    base.connect();

    return base;
};