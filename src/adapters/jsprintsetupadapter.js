var jsPrintSetupAdapter = function() {
  var base = new baseAdapter();

    base._inProgress = false;
    base._printBGColors = 0;
    base._printBGImages = 0;

    base.setPrintBGColors = function(print) {
        this._printBGColors = print;
    };

    base.setPrintBGImages = function(print) {
        this._printBGImages = print;
    };

    base.getPrinters = function() {
        var list = jsPrintSetup.getPrintersList();
        return  list.split(',');
    };

    base.print = function(html, options) {

        if (!this._printer_name) {
            return false;
        }

        if (base._inProgress) {
            setTimeout(function() {
                base.print(html, options);
            }, 2000);

            return false;
        }

        base._inProgress = true;

        if (options.orientation === 'portrait') {
            jsPrintSetup.setOption('orientation', jsPrintSetup.kPortraitOrientation);
        } else {
            jsPrintSetup.setOption('orientation', jsPrintSetup.kLandscapeOrientation);
        }

        jsPrintSetup.setPrinter(this._printer_name);

        jsPrintSetup.setOption('headerStrLeft', '');
        jsPrintSetup.setOption('headerStrCenter', '');
        jsPrintSetup.setOption('headerStrRight', '');
        jsPrintSetup.setOption('footerStrLeft', '');
        jsPrintSetup.setOption('footerStrCenter', '');
        jsPrintSetup.setOption('footerStrRight', '');

        jsPrintSetup.setOption('marginTop', options.marginTop); //this.getMarginTop());
        jsPrintSetup.setOption('marginBottom', options.marginBottom); //this.getMarginBottom());
        jsPrintSetup.setOption('marginLeft', options.marginLeft); //this.getMarginLeft());
        jsPrintSetup.setOption('marginRight', options.marginRight); //this.getMarginRight());

        jsPrintSetup.setOption('printBGColors', options.printBGColors);//this._printBGColors);
        jsPrintSetup.setOption('printBGImages', options.printBGImages);//this._printBGImages);
        jsPrintSetup.setSilentPrint(true);
        jsPrintSetup.setShowPrintProgress(false);


        if (html) {
            var iframe = document.createElement("iframe");
            document.body.appendChild(iframe);

            iframe.id = 'js-print-setup-frame';
            iframe.style = 'display: none';
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write(html);
            iframe.contentWindow.document.close();

            iframe.onload = function() {
                jsPrintSetup.printWindow(iframe.contentWindow);
                base._inProgress = false;
            };
        } else {
            jsPrintSetup.print();
            base._inProgress = false;
        }
    };

  return base;
};