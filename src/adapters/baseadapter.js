var baseAdapter = function() {
    return {
        _printer_name: null,
        _orientation: 'portrait',
        _marginTop: 0,
        _marginRight: 0,
        _marginBottom: 0,
        _marginLeft: 0,
        _units: 'mm',

    setPrinterName: function(printer_name) {
        this._printer_name = printer_name;

        return this;
    },

    setOrientation: function(orientation)
        {
            if (orientation !== 'portrait' && orientation !== 'landscape') {
                return false;
            }

            this._orientation = orientation;

            return true;
        },

        getOrientation: function()
        {
            return this._orientation;
        },

        //от верхнего, правого, нижнего и левого края.
        setMargin(top, right, bottom, left)
        {
            this.setMarginTop(top);
            this.setMarginRight(right);
            this.setMarginBottom(bottom);
            this.setMarginLeft(left);
        },

        setMarginTop: function(margin)
        {
            this._marginTop = margin;
        },

        getMarginTop: function()
        {
            return this._marginTop;
        },

        setMarginRight: function(margin)
        {
            this._marginRight = margin;
        },

        getMarginRight: function()
        {
            return this._marginRight;
        },

        setMarginBottom: function(margin)
        {
            this._marginBottom = margin;
        },

        getMarginBottom: function()
        {
            return this._marginBottom;
        },

        setMarginLeft: function(margin)
        {
            this._marginLeft = margin;
        },

        getMarginLeft: function()
        {
            return this._marginLeft;
        },

        setUnits: function(units)
        {
            this._units = units;
        },

        getUnits: function()
        {
            return this._units;
        },

        getPrinters: function() {

        },

        isConnected: function() {
            console.warn('isConnected method not defined!');
        },

        print: function(html)
        {
            console.warn('print method not defined!');
        }
    }
};