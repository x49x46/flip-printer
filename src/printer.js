var fPrinter = function(options) {
    //
    // console.log('create printer');
    //
    // if (options && options.adapters) {
    //     options.adapters.unshift('baseadapter');
    //
    //     for (var i in options.adapters) {
    //         var script = document.createElement('script');
    //         script.type = 'text/javascript';
    //         script.src = 'src/adapters/' + options.adapters[i] + '.js';
    //
    //         console.log('src: ', script.src);
    //
    //         document.head.insertBefore(script, document.head.firstChild);
    //     }
    // }

    this._marginTop = 0;
    this._marginRight = 0;
    this._marginBottom = 0;
    this._marginLeft = 0;
    this._orientation = 'portrait';

    this._providerName = '';

    /**
     * Устанавливает ориентацию печати
     *
     * @param orientation
     * @returns {boolean}
     */
    this.setOrientation = function(orientation)
    {
        if (orientation !== 'portrait' && orientation !== 'landscape') {
            return false;
        }

        this._orientation = orientation;

        return true;
    };

    /**
     * Возвращает текущую ориентацию печати
     *
     * @returns {string|*}
     */
    this.getOrientation = function()
    {
        return this._orientation;
    };

    this.setMarginTop = function(margin)
    {
        this._marginTop = margin;
    };

    this.getMarginTop = function()
    {
        return this._marginTop;
    };

    this.setMarginRight = function(margin)
    {
        this._marginRight = margin;
    };

    this.getMarginRight = function()
    {
        return this._marginRight;
    };

    this.setMarginBottom = function(margin)
    {
        this._marginBottom = margin;
    };

    this.getMarginBottom = function()
    {
        return this._marginBottom;
    };

    this.setMarginLeft = function(margin)
    {
        this._marginLeft = margin;
    };

    this.getMarginLeft = function()
    {
        return this._marginLeft;
    };

    /**
     * Устанавливает "флаг" печати фонового цвета
     *
     * @param print
     * @returns {boolean}
     */
    this.setPrintBGColors = function(print) {
        if (this._providerName !== 'jsPrintSetup') {
            return false;
        }

        this.provider.setPrintBGColors(print);
    };

    /**
     * Устанавливает "флаг" печати фонового изображения
     *
     * @param print
     * @returns {boolean}
     */
    this.setPrintBGImages = function(print) {
        if (this._providerName !== 'jsPrintSetup') {
            return false;
        }

        this.provider.setPrintBGImages(print);
    };

    this.setScale = function(scale) {
        if (this._providerName !== 'qz') {
            return false;
        }

        this.provider.setScale(scale);
    };

    /**
     * Устанавливает произвольный провайдер печати по имени
     *
     * @param provider_name string имя провайдера
     * @returns {boolean}
     */
    this.setProviderByName = function(provider_name) {
        this._providerName = provider_name;

        switch (provider_name) {
            case 'qz':
                this.setProvider(new qzAdapter());
                return true;
            case 'jsPrintSetup':
                this.setProvider(new jsPrintSetupAdapter());
                return true;
            default:
                return false;
        }
    };

    /**
     *  Устанавливает провайдер печати
     *
     * @param provider object провайдер (qzAdapter, jsPrintSetupAdapterы)
     */
    this.setProvider = function(provider)
    {
        this.provider = provider;
    };

    /**
     * Возвращает провайдер печати
     *
     * @returns {*|null}
     */
    this.getProvider = function () {
        return this.provider;
    };

    /**
     * Устанавливает имя принтера для печати
     *
     * @param printer_name
     * @returns {boolean}
     */
    this.setPrinterName = function (printer_name) {
        if (!this.provider) {
            return false;
        }

        this.provider.setPrinterName(printer_name);

        return true;
    };

    /**
     * Возвращает имя принтера
     *
     * @returns {boolean|boolean|*}
     */
    this.getPrinterName = function() {
        if (!this.provider) {
            return false;
        }

        return this.provider.getPrinterName();
    };

    /**
     * Проверяет доступность провайдеров и устанавливает согласно приоритету
     *
     * @returns {boolean}
     */
    this.checkProvider = function()
    {
        if (typeof qz != "undefined") {
            this.setProvider(new qzAdapter());
            this._providerName = 'qz';
            return true;
        }

        if (typeof jsPrintSetup != "undefined") {
            this.setProvider(new jsPrintSetupAdapter());
            this._providerName = 'jsPrintSetup';
            return true;
        }

        return false;
    };

    this.provider = (options && options.provider) ? options.provider : null;

    if (!this.provider) {
        this.checkProvider();
    }

    /**
     * Возвращает имя провайдера печати
     *
     * @returns {string|*}
     */
    this.getProviderName = function()
    {
        return this._providerName;
    };

    /**
     * Возвращает список принтеров
     *
     * @returns {*}
     */
    this.getPrinters = function () {
        return this.provider.getPrinters();
    };

    /**
     * Принтер подключён?
     *
     * @returns {*|void|never}
     */
    this.isConnected = function() {
        return this.provider.isConnected();
    };

    /**
     * Печать
     *
     * Перед печатью содержимое подвергается обработке так как страница в QZ обрабатывается автономно и не может
     * получить доступ к ресурсам (картинки, стили)
     *
     * @param html string содержимое печати
     * @param printer_name string имя принтера
     * @param config object параметры печати
     * @returns {boolean}
     */
    this.print = function (html, printer_name = null, config = null) {
        var self = this;

        if (this.provider) {

            //clear script
            html = html.replace(/(\r\n|\n|\r)/gm, '');
            html = html.replace(/<script(.*)<\/script>/ig,'');

            if (printer_name) {
                this.setPrinterName(printer_name);
            }

            if (self._providerName === 'jsPrintSetup') {
                self.provider.print(html, config);
                return true;
            }

            //prepare images start
            var iframe = document.createElement("iframe");
            document.body.appendChild(iframe);

            iframe.id = 'f-print-prepare-frame';
            iframe.style = 'display: none';
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write(html);
            iframe.contentWindow.document.close();

            //prepare images end
            $(iframe.contentWindow.document).ready(function () {

                //css link
                var included_count = 0;
                var link_count = $(iframe.contentWindow.document).find('link').length;
                var loader = new Promise(function(resolve) {
                    if (link_count === 0) {
                        resolve(false);
                    }

                    $(iframe.contentWindow.document).find('link').each(function(index, link) {
                        $.get($(link).attr('href'), function(response) {
                            if (iframe.contentWindow) {
                                response = response.replace(/(\r\n|\n|\r)/gm, '');
                                $(iframe.contentWindow.document.body).append('<style>' + response + '</style>');
                            }

                            included_count++;

                            if (included_count === link_count) {
                                resolve(true);
                            }
                        });
                    });
                });

                //convert images to base64
                var converted_count = 0;
                var img_count = $(iframe.contentWindow.document).find('img').length;
                var converter = new Promise(function(resolve) {
                    if (img_count === 0) {
                        resolve(false);
                    }

                    $(iframe.contentWindow.document).find('img').each(function(index, img) {

                        img.converted = false;

                        if (!img.converted && img.src.indexOf('base64')) {
                            converted_count++;
                            img.converted = true;

                            if (converted_count === img_count) {
                                resolve(true);
                            }

                            return true;
                        }

                        $(img).load(function() {
                            if (img.converted) {
                                converted_count++;
                            }

                            if (!img.converted && img.src.indexOf('.svg') !== -1) {
                                converted_count++;
                                img.converted = true;
                            }

                            if (!img.converted) {
                                image2base64.convert(img);
                            }

                            if (converted_count === img_count) {
                                resolve(true);
                            }
                        });
                    });
                });


                var all_complete = new Promise(function(resolve) {
                    var complete_loader = false;
                    var complete_converter = false;

                    loader.then(function() {
                        complete_loader = true;
                        if (complete_loader && complete_converter) {
                            resolve(true);
                        }
                    }, function() {
                        return console.warn('стили не загружены');
                    });

                    converter.then(function () {
                        complete_converter = true;
                        if (complete_loader && complete_converter) {
                            resolve(true);
                        }
                    }, function() {
                        console.warn('изображения не конвертированы ');
                    });
                });

                all_complete.then(function() {
                    var content = '<head>' +
                                  iframe.contentDocument.head.innerHTML +
                                  '</head>' +
                                  '<body>' +
                                  iframe.contentWindow.document.body.innerHTML +
                                  '</body>';


                    //self.provider.setOrientation(self.getOrientation());

                    //self.provider.setMarginTop(self.getMarginTop());
                    //self.provider.setMarginRight(self.getMarginRight());
                    //self.provider.setMarginBottom(self.getMarginBottom());
                    //self.provider.setMarginLeft(self.getMarginLeft());

                    self.setPrinterName(printer_name);
                    //TODO: возможно имя принтера в конфигурации предпочтительней
                    config.printer_name = printer_name;

                    self.provider.print(content, config);
                    $(iframe).remove();
                });
            });
        } else {
            console.warn('provider not defined!');
            return false;
        }
    };

    return this;
};