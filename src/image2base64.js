var image2base64 = {
    canvas: null,
    convert: function(img) {
            if (img.converted) {
                return true;
            }

            img.origin = 'anonymous';
            img.crossOrigin = "anonymous";

            if (!this.canvas) {
                this.canvas = document.createElement("canvas");
            }

            this.canvas.width = img.width;
            this.canvas.height = img.height;

            var width = img.width;
            var height = img.height;

            var ctx = this.canvas.getContext("2d");

            ctx.drawImage(img, 0, 0, img.width, img.height);

            ctx.beginPath();

            var alpha = Math.random() * 0.1;
            var r = Math.random() * 100 + 150;
            var g = Math.random() * 100 + 150;
            var b = Math.random() * 100 + 150;
            var x = Math.random() * width;
            var y = Math.random() * height;

            ctx.strokeStyle = "rgba("+r+","+g+","+b+","+alpha+")";
            ctx.moveTo(0, 0);
            ctx.lineTo(x, y);
            ctx.stroke();
            ctx.closePath();

            //TODO: IF: SVG не работает
            //The reason why you cannot use an SVG image element as source for the drawImage method is simple,
            // but painful: the current Canvas specification does not (yet) allow to reference
            // SVGImageElement as source for drawImage and can only cope with HTMLImageElement, HTMLCanvasElement and
            // HTMLVideoelement. This short-coming will hopefully be addressed during the process
            // of defining "SVG in HTML5" behavior and could be extended to allow SVGSVGElement as well.
            // The xhtml:img element in listing 3 uses visibility:hidden as we do not want it to interfere with its
            // visible copy on the Canvas.

            img.src = this.canvas.toDataURL("image/png");
            img.converted = true;
    }
};