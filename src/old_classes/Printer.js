'use strict';

// const QZAdapter = require('./adapters/QZAdapter');
// const JsPrintSetupAdapter = require('./adapters/JsPrintSetupAdapter');

class Printer
{
    /**@var provider Adapter*/
    provider = null;
    ready = false;

    constructor(options = null)
    {
        console.log('printer created');
        if (options && options.provider) {
            this.setProvider(options.provider);
        } else {
            this.ready = this.checkProvider();
        }

        if (options) {
            this.setOptions(options);
        }
    }

    setProvider(provider)
    {
        this.provider = provider;
        this.ready = true;
        return this;
    }

    setOptions(options)
    {
        this.provider.setOptions(options);

        return this;
    }

    checkProvider()
    {
        if (qz) {
            this.setProvider(new QZAdapter());
            return true;
        }

        if (jsPrintSetup) {
            this.setProvider(new JsPrintSetupAdapter());
            return true;
        }

        return false;
    }

    print(html, options = null)
    {
        if (options) {
            this.setOptions(options);
        }

        this.provider.print(html);
    }

    isReady()
    {
        return this.ready;
    }
}

//module.exports = Printer;