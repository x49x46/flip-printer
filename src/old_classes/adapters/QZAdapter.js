'use strict';

const Adapter = require('./Adapter');

export class QZAdapter extends Adapter {
    _units = 'cm';
    _connected = false;
    _promise = null;
    _cert = null;

    constructor(cert = null)
    {
        super();

        this.setCert(cert);
        this.initCrypto();

        this.connect();
    }

    connect(callback = null)
    {
        let self = this;

        if (!qz.websocket.isActive()) {
            this._promise = new Promise((resolve, reject) => {
                qz.websocket.connect({retries: 5, delay: 1}).then(function () {
                    self._connected = true;
                    resolve(true);
                    if (callback) callback();
                }).catch(function (error) {
                    console.error(error);
                    reject(false);
                });
            });
        } else {
            return self._connected = true;
        }
    }

    find(resolve)
    {
        qz.printers.find().then(resolve).catch(function(data) {
            console.error(data);
        });
    }

    exec(html)
    {
        let config = qz.configs.create('PDF', {
            units: this.getUnits(),
            margins: {
                top: this.getMarginTop(),
                right: this.getMarginRight(),
                bottom: this.getMarginBottom(),
                left: this.getMarginLeft()
            }
        });

        let data = [{
            type: 'html',
            format: 'plain', // 'plain' or 'file' if using a URL
            data: html
        }];

        qz.print(config, data).catch(function (e) {
            console.error(e);
        });
    }

    print(html) {
        let self = this;

        if (this.isConnected()) {
            this.exec(html);
        } else if (this.inConnect()) {
            this._promise.then(function(resolve) {
                if (resolve) {
                    self.exec(html);
                }
            });
        } else {
            //if QZ reload
            this.connect(function(){
                self.exec(html);
            });
        }
    }

    setCert(cert)
    {
        this._cert = cert;
        this.initCrypto();
    }

    initCrypto()
    {
        console.log('init crypto');
        let self = this;
        // Authentication setup //
        qz.security.setCertificatePromise(function (resolve, reject) {
            if (self.cert) {
                console.log('sefl cert used');
                resolve(self.cert);
            } else {
                //Preferred method - from server
                $.ajax({ url: "cert.pem", cache: false, dataType: "text" }).then(resolve, reject);
            }
        });

        qz.security.setSignaturePromise(function(toSign) {
            return function(resolve, reject) {
                $.post("crypt.php", {request: toSign}).then(resolve, reject);
            };
        });
    }

    isConnected()
    {
        return this._connected;
    }

    inConnect()
    {
        return !!this._promise;
    }
}