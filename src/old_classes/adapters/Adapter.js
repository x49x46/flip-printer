'use strict';

class Adapter
{
    _marginTop = 0;
    _marginRight = 0;
    _marginBottom = 0;
    _marginLeft = 0;
    _units = '';

    constructor()
    {
        
    }

    getUnits() {
        return this._units;
    }

    setUnits(value) {
        this._units = value;
    }

    getMarginTop() {
        return this._marginTop;
    }

    setMarginTop(value) {
        this._marginTop = value;

        return this;
    }

    getMarginRight() {
        return this._marginRight;
    }

    setMarginRight(value) {
        this._marginRight = value;

        return this;
    }

    getMarginBottom() {
        return this._marginBottom;
    }

    setMarginBottom(value) {
        this._marginBottom = value;

        return this;
    }

    getMarginLeft() {
        return this._marginLeft;
    }

    setMarginLeft(value) {
        this._marginLeft = value;

        return this;
    }

    setOptions(options)
    {
        if (options.marginLeft) {
            this.setMarginLeft(options.marginLeft);
        }

        if (options.marginRight) {
            this.setMarginRight(options.marginRight);
        }

        if (options.marginTop) {
            this.setMarginTop(options.marginTop);
        }

        if (options.marginBottom) {
            this.setMarginBottom(options.marginBottom);
        }

        if (options.units) {
            this.setUnits(options.units);
        }
    }

    print(html)
    {

    }
}